# Google Dorks


Google Dorks, a technique involving the use of advanced search operators in Google, can be an invaluable tool for cybersecurity professionals and IT administrators. By crafting specific queries, these experts can identify exposed databases, misconfigured servers, and other security vulnerabilities, allowing them to proactively secure sensitive information before it can be exploited by malicious actors. When used responsibly, Google Dorks aids in strengthening the security posture of organizations and enhancing overall cybersecurity hygiene. However, it is crucial to use this technique ethically and within legal boundaries, as unauthorized access to sensitive information can lead to serious legal consequences and potential harm. Organizations must ensure their data is adequately protected to prevent inadvertent exposure through search engines.  
 


### Before You Begin Google Dorking

Google dorking is not a playground where you can flood commands to your heart’s content


*   Google limits your Google search rate from a single device.
*   It may ban your IP if you issue too many queries.
*   Abuse of dorks may have legal repercussions.


No, you’re not immune even if you’re working from a virtual machine toying with **sqlmap**.


If you know you can’t resist having fun with it (and you will), you could work from **Pagodo**, which automates Google searching for potentially vulnerable web pages and applications on the Internet. It also lets you automate the rate at which your device issues Google dorks.


Regardless of how you use Google dorks, respect Google’s Terms of Service. Be careful.


 


### Google Dorking Operators

Lets start with some of the well known and frequently used google dorks operators.


*   **" "** : Return exact matches of a query string enclosed in the double quotes. Note that these are straight and not curly “” quotation marks. The curly quotes may or may not return similar results as straight quotes. Single quotes don’t work. Example `"Google dorking commands"`
*   **OR, |** : Return sites containing either query item joined by OR or the pipe character |.This is an inclusive OR. Example `Amazon OR Google`
*   **( )** : Group multiple Google dork operators as a logical statement. Example `(black OR white) hat hacker`
*   **\-** : Exclude search results containing the word or phrase after the hyphen. Example `"Amazon" -reviews`
*   **\*** : Wildcard or glob pattern as a placeholder for query item. Example `"type * error"` returns pages on Type I and II errors in statistics. This is comparable to search `“type i OR ii error”` which doesn’t use this wildcard
*   **#..#** : Search a numerical range specified by the two endpoints # inclusive. Example `2006..2008` finds all pages that include 2006, 2007, or 2008 in them.
*   **AROUND(N)** : Match pages containing the search terms separated by at most N other words. Example `read AROUND(2) book`


### Search Scope-Restricting Dorks


If a command listed below ends with a symbol, include no space between the command and the parameter. Otherwise, Google will treat the command as an ordinary search keyword rather than a dork.


##### **site:**


Restrict search to a particular website, top-level domain, or subdomain. Example `site:google.com`


##### **filetype: , ext:**


Restrict the returned web addresses to the designated file type. Unlike most other dorks, this requires additional keywords in the search bar or will return no results. Here is Google’s [official list](https://developers.google.com/search/docs/crawling-indexing/indexable-file-types?hl=en&visit_id=638010780742925546-492926684&rd=1) of common file types it can search. Google also supports the file extensions db, log, and html. Example`filetype:pdf car design` , `ext:log username`


##### **@**


Search to a particular social platform. It supports popular platforms such as Facebook, Twitter, YouTube, and Reddit. It is not as precise as the **site:** Restrictdork. Example `@youtube google dorking`


##### **stocks:**


Check the financial activity of a particular stock. Example `stocks:META` (Meta)


##### **define:**


Return definitions of a word or phrase. Example `define:privacy`


##### **movie:**


Return information about any movie with the given title. Example `movie:"phantom of the opera"`


##### **source:**


Find reports from a Google News source. Example `source:cnn`


### Informational Dorks

These dorks appear to work best if used as standalone commands, i.e., without additional query items.


##### **$**


Search for prices in USD ($). This also works for Euro (€), but not GBP (£) or Yen (¥). Example `ipad $329, iphone €239`


##### **cache:**


Get Google’s last saved version of a particular website. A website snapshot like this is called “cache”. Example `cache:news.yahoo.com`


##### **link:**


Find pages linking to the given domain. Example `link:www.laniak.net`


##### **related:**


Return websites related to the given website. Example `related:cousera.com`


##### **map:**


Get a map of the given location. Example `map:"Mumbai"`


##### **weather:**


Get the weather of the given location. Example `weather:India`


### Text Dorks


These are helpful if you want to look for web pages containing certain text strings or follow particular patterns.


##### **intitle: , allintitle:**


Look for pages with titles containing the search terms.The dork “intitle:” applies to its search parameter only, while “allintitle:” applies to the entire query string. Example `intitle:toy story` , `intitle:"toy story"` , `allintitle:"toy story"`


##### **inurl:**


Find links containing the character string. Example `inurl:login.php`


##### **allinurl:**


Find links containing all words following the colon (:). Example `allinurl: healthy eating`


### Complex Google Dorks

You can combine Google dorking commands and operations for specific results.


*   **inurl:zoom.us/j intext:scheduled** : Get links to publicly shared Zoom meetings you may want to access.
*   **"index of" "database.sql.zip"** : Get unsecured SQL dumps. Data from improperly configured SQL servers will show up on this page.
*   **filetype:yaml inurl:cassandra** : Get YAML configuration files specific to _Apache Cassandra databases_
*   **@youtube trending shorts** : Find short clips trending on YouTube
*   **@reddit memes -dark** : Find memes on Reddit that are not dark
*   **site:cdn.cloudflare.net filetype:pdf** : Find PDFs on the \*.cdn.cloudflare.net domain
*   **secret in spanish inurl:dict** : Translate the word “secret” to Spanish and limit results to URLs containing “dict”
*   **filetype:doc site:www.laniak.com Shashank** : laniak with the .doc extension. This looks for legacy Microsoft Word files containing the keyword “Shashank”.


### Reminder to be cautious

With great power comes great responsibility, and even if you use Google Dorks with the utmost care, other entities may not. Here are some suggestions to avoid becoming the next victim of unwanted Google Dorking.


*   Implement IP-based restrictions and password authentication to protect private areas. Securing your login portals discourages unauthorized access.
*   Encrypt all sensitive information, like usernames, passwords, email addresses, phone numbers, and physical addresses. This way, in the event of data leakage, the original data remains unexposed.
*   Run vulnerability scans to find and disable Google dorks. Examples of vulnerability scanners are nmap, Nessus, and Qualys.
*   Run regular dork queries on your website to discover loopholes and sensitive information before attacks occur. Sqlmap is a helpful tool.
*   If you find sensitive content exposed on your website and you’ve exhausted all other means of removing it (such as changing your passwords or renaming your login pages), request its removal through Google Search Console.
*   Be judicious in the use of robots.txt.


Regarding robots.txt, what seems like a simple, good-faith solution to eliminate complex reconnaissance via Google Dorks is, to an intelligent hacker, a treasure trove and a cash cow. Instead of backing off, they’ll attack your website by targeting the items listed in robots.txt.


Hence, it’s best to adopt this measure cautiously. The most prudent use of robots.txt is instructing Google to exclude one’s entire website, as follows:

```
User-agent: *
Disallow: /
```
    
    
   


Such a robots.txt file compels visitors looking for information to use the search function inside the website. A well-built internal search function may have safeguards against Google dorking, SQL injection, and other hacking techniques. These safeguards protect the website better than allowing external search engines such as Google to index the website.


### Conclusion


In conclusion, while Google Dorks can be a powerful tool for enhancing cybersecurity by identifying and addressing potential vulnerabilities, it must be used with caution and integrity. Ethical application of this technique can significantly contribute to the protection of sensitive information and the overall security of online environments. However, it is imperative to recognize the legal and ethical boundaries associated with its use to prevent misuse and potential legal repercussions. By remaining vigilant and responsible, individuals and organizations can harness the benefits of Google Dorks while safeguarding against its risks.