# Authentication in NextJs app with Azure AD B2C (Part 2)  


In part 1, we configured Azure AD B2C tenant for authentication purpose. Now let's take a look at how we are going to configure our NextJs app to integrate with our tenant. Follow the below steps to create integration between your app and Azure services.


### Download packages


The Microsoft Authentication Library (MSAL) enables developers to acquire security tokens from the Microsoft identity platform to authenticate users and access secured web APIs. It can be used to provide secure access to Microsoft Graph, other Microsoft APIs, third-party web APIs, or your own web API. MSAL supports many different application architectures and platforms including .NET, JavaScript, Java, Python, Android, and iOS.


Use below command to download necessary npm packages


`npm install @azure/msal-browser @azure/msal-react`


### Register msal global instance


Once you installed the necessary dependencies, next step is to create msal instace which will be used by components to communicate with Azure services. Go to the root js file of your app and create the msal instance as shown below. For NextJs project, the root js file will be page.js under /app folder.


<img alt="Root Js Config" src="https://gitlab.com/shashankkawle/DOCS/-/raw/master/_ARTICLES/Assets/Azure_AD_B2C_2_root_config.png" style="margin-left: auto; margin-right: auto; display: block;" width="90%" height="90%" />


### Create authConfig.js file


This file will be in **/app** folder and will have all the details required to connect to Azure B2C tenant. Below is the sample authConfig file


<img alt="Root Js Config" src="https://gitlab.com/shashankkawle/DOCS/-/raw/master/_ARTICLES/Assets/Azure_AD_B2C_2_auth_config_code.png" style="margin-left: auto; margin-right: auto; display: block;" width="90%" height="90%" />


In above code, value for clientId , knownAuthorities & scopes can be retrieved from B2C tenant portal. Value for Authority is _https://(your-tenant-name).b2clogin.com/(your-tenant-name).onmicrosoft.com/(your-flow-name)_. Also note that value in **redirectUri** should match with any values present in redirect uris present in Azure B2C Tenant config in Azure Portal.


### Create Login Component


Not that we have all the config in place, let's focus on login component. In this component, we will use msal instance in login method to connect with B2C tenant and validate credentials. Refer below code.


<img alt="Root Js Config" src="https://gitlab.com/shashankkawle/DOCS/-/raw/master/_ARTICLES/Assets/Azure_AD_B2C_2_login.png" style="margin-left: auto; margin-right: auto; display: block;" width="90%" height="90%" />


*   On line 6, we have imported our authConfig in **loginRequest**variable
*   On line 13, we are fetching our msal instance created in root js file.
*   On line 19, we are getting currently active account from msal instance.
*   We have created **handleLoginPopup** function which will get triggered on login button click. In this function, we are calling `loginPopup()` method from msal instance which will open login window as popup format. Note that there is also functionality to open login page on new tab with the help of `loginRedirect()` method.
*   Once we validated the creds and received account data in response, we are setting auth cookie by calling **/api/auth** endpoint on line 27. This is required to secure our private endpoints.
*   On line 28, we are navigating to our /workspace secured endpoint.


Also note that in JSX section, we have used **UnauthenticatedTemplate** from Msal package which is used to display contents which user is not logged in. The overall flow of our login process will be like below.


<img alt="Root Js Config" src="https://gitlab.com/shashankkawle/DOCS/-/raw/master/_ARTICLES/Assets/Azure_AD_B2C_2_navigation_flow.png" style="margin-left: auto; margin-right: auto; display: block;" width="90%" height="90%" />

  
<br/>  

### Configure api and middleware


This is the last and very simple part of our process. When our api will get triggered from login component, we will be setting auth cookie with account data that we have received from Azure AD service. When we will navigate from login component to our secured endpoint, we will check for that cookie. If the cookie is present, we will proceed with navigation else we are redirecting to login page. Below are the code snippets for api and middleware.

<img alt="Root Js Config" src="https://gitlab.com/shashankkawle/DOCS/-/raw/master/_ARTICLES/Assets/Azure_AD_B2C_2_route_and_middleware.png" style="margin-left: auto; margin-right: auto; display: block;" width="90%" height="90%" />
  
<br/>  

### Things to remember


This is just basic authentication process with default credentials login with Azure AD B2C service. Azure AD B2C service also provides OAuth configuration for other Identity providers such as Google or Facebook. Also while testing if you have already performed login one time and want to test the process again, you will need to manually clear _sessionStorage_ or _localStorage_ (based on what you have configured in authConfig.js) and cookies. You can follow same cleanup process while creating user logout flow.


For us, this concludes the integration process of Azure AD B2C tenant for authentication with our NextJs app. As I have said before, this is just basic implementation of process and not enterprise level code. There are many more possibilities with Azure AD service which we can use for other operations. Take your time to understand these services to get optimum output for your requirements.
